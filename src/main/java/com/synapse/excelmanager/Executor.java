package com.synapse.excelmanager;

/**
 * Created by Synapse Solutions on 3/30/17.
 */
public class Executor {

    public static void main(String[] args) throws Exception {

        new ExcelWorkbook().ReadBook();
        new ExcelWorkbook().WriteBook();

    }

}
