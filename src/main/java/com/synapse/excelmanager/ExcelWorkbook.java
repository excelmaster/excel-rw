package com.synapse.excelmanager;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by Synapse Solutions on 3/30/17.
 */
public class ExcelWorkbook {

    static XSSFRow row;

    public void ReadBook()throws Exception
    {
        FileInputStream fis = new FileInputStream(
                new File("uploadworkbook.xlsx"));
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet spreadsheet = workbook.getSheetAt(0);
        Iterator< Row > rowIterator = spreadsheet.iterator();
        while (rowIterator.hasNext())
        {
            row = (XSSFRow) rowIterator.next();
            Iterator <Cell> cellIterator = row.cellIterator();
            while ( cellIterator.hasNext())
            {
                Cell cell = cellIterator.next();
                switch (cell.getCellType())
                {
                    case Cell.CELL_TYPE_NUMERIC:
                        System.out.print(
                                cell.getNumericCellValue() + " \t\t " );
                        break;
                    case Cell.CELL_TYPE_STRING:
                        System.out.print(
                                cell.getStringCellValue() + " \t\t " );
                        break;
                }
            }
            System.out.println();
        }
        fis.close();
    }

    public void  WriteBook()throws Exception
    {
        //Create blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();
        //Create a blank sheet
        XSSFSheet spreadsheet = workbook.createSheet(
                " Employee Info ");
        //Create row object
        XSSFRow row;
        //This data needs to be written (Object[])
        Map< String, Object[] > empinfo =
                new TreeMap< String, Object[] >();
        empinfo.put( "1", new Object[] {
                "FILE NAME", "SUPPLIER NAME", "FILE LOCATION", "CROP STAGE" });
        empinfo.put( "2", new Object[] {
                "CP001E", "ABCD", "/var/lib/media/mAgri/1/2/3/PFS018.wav", "Fertilizing" });
        empinfo.put( "3", new Object[] {
                "CP002E", "EFGH", "/var/lib/media/mAgri/1/2/3/PFS020.wav", "Fertilizing" });
        empinfo.put( "4", new Object[] {
                "CP003E", "IJKL", "/var/lib/media/mAgri/1/2/3/PFS023.wav", "Harvesting" });
        empinfo.put( "5", new Object[] {
                "CP004E", "MNOP", "/var/lib/media/mAgri/1/2/3/PFS024.wav", "Harvesting" });

        //Iterate over data and write to sheet
        Set< String > keyid = empinfo.keySet();
        int rowid = 0;
        for (String key : keyid)
        {
            row = spreadsheet.createRow(rowid++);
            Object [] objectArr = empinfo.get(key);
            int cellid = 0;
            for (Object obj : objectArr)
            {
                Cell cell = row.createCell(cellid++);
                cell.setCellValue((String)obj);
            }
        }
        //Write the workbook in file system
        FileOutputStream out = new FileOutputStream(
                new File("Writesheet.xlsx"));
        workbook.write(out);
        out.close();
        System.out.println(
                "Writesheet.xlsx written successfully" );
    }

}
